# Class: rsyslog::install
class rsyslog::install inherits rsyslog {

    package { $rsyslog::rsyslog_package:
      ensure  => $rsyslog::rsyslog_package_ensure,
      notify  => Class['rsyslog::config'],
    }
}
