# == Class: rsyslog::config
class rsyslog::config inherits rsyslog {

    file { $rsyslog::conf_file:
      ensure  => $rsyslog::conf_file_ensure,
      owner   => $rsyslog::conf_file_owner,
      group   => $rsyslog::conf_file_group,
      mode    => $rsyslog::conf_file_mode,
      content => template("${rsyslog::conf_file_template}"),
      notify  => Class['rsyslog::service'],
      require => Class['rsyslog::install'],
    }

}
