# == Class: rsyslog::service
class rsyslog::service inherits rsyslog {

    service { $rsyslog::service:
      ensure      => $rsyslog::service_ensure,
      enable      => $rsyslog::service_enable,
      hasstatus   => $rsyslog::service_hasstatus,
      hasrestart  => $rsyslog::service_hasrestart,
      require     => Class['rsyslog::config'],
    }
}
