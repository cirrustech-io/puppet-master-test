# == Class: rsyslog
#
# === Variables
#
# === Authors
#
# Mohamed Waqqas <mohamed.waqqas@2connect.net>
#
# === Copyright
#
# Copyright 2016 2Connect W.L.L., unless otherwise noted.
#
class rsyslog ( 
  $rsyslog_package         = hiera( 'rsyslog::install::package::pkg_rsyslog' ),
  $rsyslog_package_ensure  = hiera( 'rsyslog::install::package::ensure' ),
  $conf_file                = hiera( 'rsyslog::config::file::conf_file' ),
  $conf_file_ensure         = hiera( 'rsyslog::config::conf_file::ensure' ),
  $conf_file_owner          = hiera( 'rsyslog::config::conf_file::owner' ),
  $conf_file_group          = hiera( 'rsyslog::config::conf_file::group' ),
  $conf_file_mode           = hiera( 'rsyslog::config::conf_file::mode' ),
  $conf_file_template       = hiera( 'rsyslog::config::conf_file::template' ),
  $service               = hiera( 'rsyslog::service::srv::srv' ),
  $service_ensure        = hiera( 'rsyslog::service::srv::ensure' ),
  $service_enable        = hiera( 'rsyslog::service::srv::enable' ),
  $service_hasstatus     = hiera( 'rsyslog::service::srv::hasstatus' ),
  $service_hasrestart    = hiera( 'rsyslog::service::srv::hasrestart' ),
) {

  contain rsyslog::install
  contain rsyslog::config
  contain rsyslog::service
  #class { 'rsyslog::install': }
  #class { 'rsyslog::config': }
  #class { 'rsyslog::service': }

}
